'use strict';
var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var pump = require('pump');
var dependencies = require('gulp-web-dependencies');
var glue = require("gulp-sprite-glue");
var path_dest = 'dist';
var htmlmin = require('gulp-htmlmin');
var tinfier = require('gulp-tinifier');
var gulpCopy = require('gulp-copy');
var gulpClean = require('gulp-clean');

gulp.task('copy', function(){
    return gulp.src('source/css/*.css')
               .pipe(gulpCopy(path_dest + '/css'))
               .pipe(gulp.dest(path_dest))
});

gulp.task("tinypng", function(done) {
    gulp.src('source/image/*.{jpg,png}')
        .pipe(tinfier({
            key:'VD2snCbfsBKuVI8z28tJjJugHGnDFZ0t',
            verbose: true
        }))
        .pipe(gulp.dest("image", {cwd:path_dest}))
})
 
gulp.task('dependencies', function() {
    return gulp.src('source/index.html')
        .pipe(dependencies({
            folder: "bower_components|node_modules",
            dest: path_dest,
            prefix: '/vendor',
            flat: true
        }))
        .pipe(gulp.dest(path_dest));
});
 
gulp.task('compressJs', function (cb) {
  pump([
        gulp.src('./source/js/*.js'),
        uglify(),
        gulp.dest(path_dest+'/js')
    ],
    cb
  );
});
 
gulp.task('scripts', function() {
  return gulp.src('./source/js/*.js')
    .pipe(concat('main.js'))
    .pipe(gulp.dest(path_dest + '/js'));
});

gulp.task('minifyHtml', function() {
  return gulp.src('dist/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('dist'));
});

gulp.task('cleanDistFolder', function(){
   return gulp.src('./dist')
    .pipe(gulpClean());
});

gulp.task("build", ['cleanDistFolder', 'compressJs', 'dependencies', 'minifyHtml', 'copy']);