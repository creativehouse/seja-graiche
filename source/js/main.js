
function execmascara(){
  v_obj.value=v_fun(v_obj.value);
}

function mascara(o,f){
  v_obj=o;
  v_fun=f;
  setTimeout("execmascara()",1);
}

function mtel(v){
  v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
  v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
  v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
  return v;
}

jQuery(document).ready(function($) {  

  if (document.body.getBoundingClientRect().width <= 1024) {
    $(".flipbox__item").on("click", function(){
      $(".flipbox__item.active").each(function(index, el) {
        console.log('el', el);
        $(el).removeClass("active");
      });
      $(this).addClass("active");
    });
  }

  $('a[href^="#"]').each(function(index, value){
    value.addEventListener("click", smoothScroll);
  });

  function smoothScroll(e){
    e.preventDefault();
    var container = this.href.split("#");
    $('html, body').stop().animate({
      scrollTop: $("#" + container['1']).offset().top,
      }, 400);
    return false;
  }

  $(".contato__wrapper .link__box").on("click", function(){
    $(".contato__wrapper .link__box").removeClass('active');
    $(this).addClass('active');
  })

  $("#tel").on("keyup", function(){ 
    mascara( this, mtel );
  });

  $("#frmContato").on("submit", function(e){
    e.preventDefault();
    $("#frmContato").append('<div class="loader"></div>');
    if (typeof($("input[name=perfil]:checked").val()) === "undefined") {
      alert("Preencha todos os campos, inclusive a Preferência de contato, por favor. ");
        $(".loader").remove();
      return false;
    }


    window.setTimeout(function(){
        __ss_noform.push(['submit', null, '88afa616-bf95-4eb9-a69c-6b6264fa3f3a']);
        $(".contato-answer").removeClass("hide");
        $(".contato--form").hide();
        $("#frmContato")[0].reset();
        $(".loader").remove();
        ga('send', 'event', 'form-seja-graiche', 'envio-formulario');
      }, 1500);
   return false; 
  });
});

