<?php
if (isset($_POST) && !empty($_POST)) {
    $name       = $_POST['nome'];
    $email      = $_POST['email'];
    $phone      = $_POST['tel'];
    $course     = $_POST['cursos'];

    //Inclui o arquivo class.phpmailer.php localizado na pasta phpmailer
    require("class/PHPMailerAutoload.php");

    //Inicia a classe PHPMailer
    if ($mail = new PHPMailer()){
        //Define os dados do servidor e tipo de conexão
        $mail->IsSMTP(); 
        $mail->Port = 587;  
        $mail->Host = "smtp.vanzolini.org.br"; // Endereço do servidor SMTP
        $mail->SMTPAuth = true; // Autenticação
        $mail->Username = 'naoresponder@vanzolini.org.br'; // Usuário do servidor SMTP
        $mail->Password = '@fcav2017@'; // Senha da caixa postal utilizada
        $mail->SMTPDebug = false;

        //Define o remetente
        $mail->From = "atendimento@vanzolini.org.br";
        $mail->FromName = "Comunicação Vanzolini";

        //Define os destinatário(s)
        $mail->AddAddress('rhamses.soares@gmail.com', 'Rhamses Soares');
        // if ($_SERVER['HTTP_HOST'] == 'creativehouse.com.br' || $_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '192.168.0.2') {
            
        // }

        $mensagem = '<strong>Os dados do lead seguem abaixo:</strong> <br>
                    <table>
                        <tr>
                            <td>Nome:</td>
                            <td>'.$name.'</td>
                        </tr>
                        <tr>
                            <td>Email:</td>
                            <td>'.$email.'</td>
                        </tr>
                        <tr>
                            <td>Telefone:</td>
                            <td>'.$phone.'</td>
                        </tr>
                        <tr>
                            <td>Curso:</td>
                            <td>'.$course.'</td>
                        </tr>
                    </table>';

        //Define os dados técnicos da Mensagem
        $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
        $mail->CharSet = 'UTF-8'; // Charset da mensagem (opcional)

        //Texto e Assunto
        $mail->Subject  = 'Um lead interessado em '.$course.' entrou em contato pela Landing Page'; // Assunto da mensagem
        $mail->Body     = $mensagem;
        $mail->AltBody  = $mensagem;

        //Envio da Mensagem
        $enviado = $mail->Send();

        //Limpa os destinatários e os anexos
        $mail->ClearAllRecipients();
        $mail->ClearAttachments();
        //Exibe uma mensagem de resultado
        if ($enviado) {
          $response = array("status" => true, "mensagem" => "Mensagem enviada com sucesso");
        } else {
          $response = array("status" => false, "mensagem" => "Não foi possível enviar o e-mail", "report" => $mail->ErrorInfo);
        }
        echo json_encode($response);
    }else{
        echo "Erro declarando a classe";
    }
} else {
    die(redirect());
}

function redirect(){
    $url = str_replace("newmail.php", "", $_SERVER['REQUEST_URI']);
    echo "Dados Inválidos. Você será redirecionado para preencher novamente";
    header("Location: " . $url);
}